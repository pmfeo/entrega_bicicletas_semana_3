var Bicicleta = require('../models/bicicleta');


/**
 * 
 * @param {*} req 
 * @param {*} res 
 * 
 * listar bicicletas
 * 
 */
exports.bicicleta_list = function (req, res) {
    res.render('bicicletas/index', {bicis: Bicicleta.allBicis});
}

// vista bicicletas create
exports.bicicleta_create_get = function (req, res) {
    res.render('bicicletas/create');
}
