var express = require('express');
var router = express.Router();
var bicicletaController = require('../controllers/bicicleta');
var bicicletaControllerAPI = require('../controllers/api/bicicletaControllerAPI');


// LIST
router.get('/', bicicletaControllerAPI.bicicleta_list);

router.get('/create', bicicletaController.bicicleta_create_get);

module.exports = router;