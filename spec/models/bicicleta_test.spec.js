var mongoose = require('mongoose');

// importar módulo
var Bicicleta = require('../../models/bicicleta');
// const bicicleta = require('../../models/bicicleta');


describe('Testing bicicletas', () => {
    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        // mongoose.Promise = global.Promise;
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error'));
        db.once('open', function () {
            console.log('We are connected to test database');
            done();
        });
    });


    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            mongoose.disconnect().then(() => {
                console.log('Disconnecting from test database');
                done();
            });
        });
    });


    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "verde", "carrera", [-34.5, -56.23]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("carrera");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-56.23);
        })
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacio', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('agrega una sola bici', (done) => {
            var aBici = new Bicicleta({
                "code": 1,
                "color": "verde",
                "modelo": "bmx",
                "ubicacion": [-34.34, -56.25]
            });
            Bicicleta.add(aBici, function (err, newBici) {
                if (err) console.log(err);
                Bicicleta.allBicis((err, bicis) => {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                })
            });
        });
    });


    describe('Bicicleta.findByCode', () => {
        it('devuelve una bici buscando por su código', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                var aBici = new Bicicleta({
                    "code": 1,
                    "color": "verde",
                    "modelo": "bmx",
                    "ubicacion": [-34.34, -56.25]
                });
                // console.log(aBici);
                Bicicleta.add(aBici, function(err, newBici) {
                    if (err) console.log(err);
                    var aBici2 = new Bicicleta({
                        "code": 2,
                        "color": "rojo",
                        "modelo": "montaña",
                        "ubicacion": [-34.00, -56.00]
                    });
                    Bicicleta.add(aBici2, function(err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function(err, targetBici) {
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                        });
                        done();
                    });
                });
            });
        });
    });




});





// // resetear colección antes de cada test
// beforeEach( () => {
//     Bicicleta.allBicis = [];
// })

// // elemento a testear
// describe("BicicletaAllBicis", () => {
//     // que es lo que queremos probar
//     it("comienza vacia", () => {
//         // expectativa(propiedad o valor a testear).resultado(valor esperado)
//         expect(Bicicleta.allBicis.length).toBe(0);
//     });
// });

// describe('Bicicleta.add', () => {
//     it("agregamos una nueva bici", () => {
//         // estado previo, comienza en 0
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var a = new Bicicleta(1, 'rojo', 'urbana', [-34.893667, -56.149109]);
//         Bicicleta.add(a);

//         // estado esperado, termina en 1
//         expect(Bicicleta.allBicis.length).toBe(1);
//         // esperamos que la primer posición del array sea el elemento que acamos de agregar
//         expect(Bicicleta.allBicis[0]).toBe(a);
//     });
// });

// describe('Bicicleta.findById', () => {
//     it('debe devolver la bici con id 1', () => {
//         // estado previo, comienza en 0
//         // agregamos dos bicis al array
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var a = new Bicicleta(1, 'rojo', 'urbana', [-34.893667, -56.149109]);
//         var b = new Bicicleta(2, 'blanca', 'urbana', [-34.893500, -56.154956]);
//         Bicicleta.add(a);
//         Bicicleta.add(b);

//         var targetBici = Bicicleta.findById(1);
//         expect(targetBici.id).toBe(1);
//         expect(targetBici.color).toBe(a.color);
//         expect(targetBici.modelo).toBe(a.modelo);
//         expect(targetBici.ubicacion).toBe(a.ubicacion);
//     });
// });


// describe('Bicicleta.removeById', () => {
//     it('borra una bicicleta por con el id 2', () => {
//         // estado previo, comienza en 0
//         // agregamos dos bicis al array
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var a = new Bicicleta(1, 'rojo', 'urbana', [-34.893667, -56.149109]);
//         var b = new Bicicleta(2, 'blanca', 'urbana', [-34.893500, -56.154956]);
//         Bicicleta.add(a);
//         Bicicleta.add(b);

//         Bicicleta.removeById(2);

//         expect(Bicicleta.allBicis.length).toBe(1);
//         expect(Bicicleta.allBicis[0].id).not.toBe(!2);        
//     });
// });