const {
    model
} = require("../../models/reserva");

var mongoose = require('mongoose');

var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');
var Bicicleta = require("../../models/bicicleta");

describe('Testing USUARIOS', () => {

    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        // mongoose.Promise = global.Promise;
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error'));
        db.once('open', function () {
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function (done) {
        Reserva.deleteMany({}, (err, success) => {
            if (err) console.log(err);
            Usuario.deleteMany({}, (err, success) => {
                if (err) console.log(err);
                Bicicleta.deleteMany({}, (err, success) => {
                    if (err) console.log(err);
                    done();
                })
            })
        })
    });


    describe('Cuando un usuario reserva una bici', () => {
        it('debe existir una reserva', (done) => {
            const usuario = new Usuario({
                nombre: "Fulano"
            });
            usuario.save();

            // console.log(usuario);

            const bicicleta = new Bicicleta({
                code: 1,
                color: "rojo",
                modelo: "montaña"
            });
            bicicleta.save();

            // console.log(bicicleta._id);

            var hoy = new Date();
            var mañana = new Date();
            mañana.setDate(hoy.getDate() + 1);

            usuario.reservar(bicicleta._id, hoy, mañana, function (err, reserva) {
                console.log(reserva);
                Reserva.find({}).populate({path: 'bicicleta', model: 'Bicicleta'}).populate({path: 'usuario', model: 'Usuario'}).exec(function (err, reservas) {
                    if (err) console.log(err);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                })
            })
        })
    })

})